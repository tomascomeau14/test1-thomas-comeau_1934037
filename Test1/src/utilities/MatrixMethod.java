package utilities;

public class MatrixMethod {
	
	public int[][] duplicate(int[][] arr){
		int awidth = arr.length;
		int alength = arr[0].length;
		alength = alength*2;
		int[][] dup = new int[awidth][alength];
		for(int i=0;i<arr.length;i++) {
			for(int o=0;o<arr[i].length;o++) {
				dup[i][o] = arr[i][o];
			}
			int pos = dup[0].length/2;
			for(int o=0;o<arr[i].length;o++) {
				dup[i][pos] = arr[i][o];
				pos++;
			}
		}
		return dup;
	}
}
