package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		int [][] arr = {{1,2,3},{4,5,6}};
		int [][] dup = new MatrixMethod().duplicate(arr);
		assertEquals(1,dup[0][0]);
		assertEquals(2,dup[0][1]);
		assertEquals(3,dup[0][2]);
		assertEquals(1,dup[0][3]);
		assertEquals(2,dup[0][4]);
		assertEquals(3,dup[0][5]);
		assertEquals(4,dup[1][0]);
		assertEquals(5,dup[1][1]);
		assertEquals(6,dup[1][2]);
		assertEquals(4,dup[1][3]);
		assertEquals(5,dup[1][4]);
		assertEquals(6,dup[1][5]);
	}

}
