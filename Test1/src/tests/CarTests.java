package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void testLocation() {
		Car c = new Car(50);
		assertEquals(0,c.getLocation());
	}
	@Test
	void testSpeed() {
		Car c = new Car(0);
		assertEquals(0,c.getSpeed());
	}
	@Test
	void testCar() {
		Car c = new Car(50);
		assertEquals(50,c.getSpeed());
		assertEquals(0,c.getLocation());
	}
	@Test
	void testMoveRight() {
		Car c = new Car(50);
		c.moveRight();
		assertEquals(50,c.getLocation());
	}
	@Test
	void testMoveLeft() {
		Car c = new Car(50);
		c.moveLeft();
		assertEquals(-50,c.getLocation());
	}
	@Test
	void testAccelerate() {
		Car c = new Car(50);
		c.accelerate();
		assertEquals(51,c.getSpeed());
	}
	@Test
	void testDecelerate() {
		Car c = new Car(50);
		c.decelerate();
		assertEquals(49,c.getSpeed());
		Car c1 = new Car(0);
		c.decelerate();
		assertEquals(0,c1.getSpeed());
	}

}
